package sk.murin.puzzle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Puzzle implements ActionListener {

    JButton[] buttons = new JButton[9];
    private final ImageIcon[] poleIcon = {new ImageIcon("0.jpg"), new
            ImageIcon("1.jpg"), new ImageIcon("2.jpg"),
            new ImageIcon("3.jpg"), new
            ImageIcon("4.jpg"), new ImageIcon("5.jpg")
            , new ImageIcon("6.jpg"), new
            ImageIcon("7.jpg"), new ImageIcon("8.jpg")
    };
    private final Random r = new Random();

    public Puzzle() {
        JFrame frame = new JFrame("Puzzle");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridLayout(3, 3));

        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new JButton();
            buttons[i].setName(String.valueOf(i));
            buttons[i].addActionListener(this);
            frame.add(buttons[i]);
        }
        JMenuBar menuBar = new JMenuBar();
        JMenu menuOptions = new JMenu("Options");

        menuBar.add(menuOptions);

        JMenuItem itemFullImg = new JMenuItem("Show full img");
        itemFullImg.addActionListener(e -> showImg());
        menuOptions.add(itemFullImg);
        frame.setJMenuBar(menuBar);

        frame.setResizable(false);
        frame.setVisible(true);
        for (int i = 0; i < poleIcon.length; i++) {
            poleIcon[i] = getScaledImage(poleIcon[i], frame.getWidth() / 3, frame.getHeight() / 3);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int randomNumb = r.nextInt(9);
        JButton buttonPressed = (JButton) e.getSource();
        buttonPressed.setIcon(poleIcon[randomNumb]);
        check();
    }

    private ImageIcon getScaledImage(ImageIcon srcImg, int w, int h) {
        Image image = srcImg.getImage();
        Image newimg = image.getScaledInstance(w, h, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(newimg);

    }

    public void showImg() {
        JFrame frameImg = new JFrame(" River ");
        frameImg.setLocationRelativeTo(null);
        frameImg.setLayout(null);
        frameImg.setSize(500, 500);

        ImageIcon ii = new ImageIcon("river.jpg");
        JLabel label = new JLabel(ii);
        label.setBounds(0, 0, 500, 500);
        frameImg.add(label);

        frameImg.setResizable(false);
        frameImg.setVisible(true);
    }

    public void check() {
        for (int i = 0; i < buttons.length; i++) {
            if (buttons[i].getIcon() == poleIcon[i]) {
                buttons[i].setEnabled(false);
            }
        }
    }
}
